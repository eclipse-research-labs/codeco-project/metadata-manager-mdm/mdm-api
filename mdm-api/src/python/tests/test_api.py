"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import unittest

import mdm

import flask

from app import app

app.app.testing = True

class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')


  

    def test_checkInforequest(self):
        print(flask.Response(mdm.get()).status_code)
        self.assertEqual(flask.Response(mdm.get()).status_code, 200)


    def test_get(self):
        res = app.app.test_client().get('mdm/api/v1/')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.get_json()['Software'], 'MetaData-Manager MDM')

if __name__ == '__main__':
    unittest.main()
