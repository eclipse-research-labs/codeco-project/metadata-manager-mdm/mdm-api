"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import json
import flask

def get():
    info={}
    info["Verison"] = "beta g.0.1"
    info["Software"] = "MetaData-Manager MDM"
    info["Name"] = "MetaData-Manager MDM API"
    json_result = json.dumps(info)
    return flask.Response(response=json_result, status=200, content_type='application/json')