"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import json
import flask
import logging
from neo4j import GraphDatabase
import ssl
import time

from kafka.admin import KafkaAdminClient, NewTopic
from kafka import KafkaConsumer
import config

def get():
    info={}
    info["Verison"] = "beta a.0.1"
    info["Software"] = "MetaData-Manager MDM"
    info["Name"] = "MetaData-Manager MDM API"
    json_result = json.dumps(info)
    return flask.Response(response=json_result, status=200, content_type='application/json')


def state():
    try:
        driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
        neo4jSession = driver.session()
        cypher = "MATCH (n) RETURN distinct labels(n) as LABELS"
        results = neo4jSession.run(cypher)
        n4j = "ok"
    except Exception as error:
        print(error)
        n4j = str(error)
    

    state={}
    state["state"] ="ok"
    state["kafa-broker"] = "ok"
    state["zookeeper"] = "ok"
    state["neo4j"] = n4j
    state["mariadb"] = "ok"
    json_result = json.dumps(state)
    return flask.Response(response=json_result, status=200, content_type='application/json')




def cleanall():
    #https://mukherjeesankar.wordpress.com/2020/04/18/how-to-apache-kafka-with-kafka-python/
    print("###############")
    sasl_mechanism = 'SCRAM-SHA-512'
    security_protocol = 'SASL_SSL'
    # Create a new context using system defaults, disable all but TLS1.2
    context = ssl.create_default_context()
    context.options &= ssl.OP_NO_TLSv1
    context.options &= ssl.OP_NO_TLSv1_1
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    try:
        kafkaAdmin = KafkaAdminClient(
                 #bootstrap_servers= config.KAFKA_BROKER,
                 bootstrap_servers=[
                'mdm-kafka-headless:9092'
            ],
                 sasl_plain_username = config.KAFKA_USER,
                 sasl_plain_password = config.KAFKA_PASSWORD,
                 ssl_context = context,
                 api_version=(2, 5, 0),
                 security_protocol = security_protocol,
                 sasl_mechanism = sasl_mechanism
                 )

        kafkaClient = KafkaConsumer(
            bootstrap_servers=[
                'mdm-kafka-headless:9092'
            ],
                 sasl_plain_username = config.KAFKA_USER,
                 sasl_plain_password = config.KAFKA_PASSWORD,
                 ssl_context = context,
                 api_version=(2, 5, 0),
                 security_protocol = security_protocol,
                 sasl_mechanism = sasl_mechanism

        )
    except Exception as e:
        logging.error(e)

    topicProgress= False
    try:
        fs = kafkaAdmin.delete_topics(topics=[config.KAFKA_TOPIC],timeout_ms=1000)
        topicProgress= True
    except Exception as e:
        logging.error(e)
    while topicProgress and (config.KAFKA_TOPIC in kafkaClient.topics()):
        time.sleep(3)
        logging.info("wait until topic is deleted")



    topic = NewTopic(config.KAFKA_TOPIC, 1, 1, topic_configs={'cleanup.policy': 'compact'})
    try:
        fs =  kafkaAdmin.create_topics([topic])
    except Exception as e:
        logging.error(e)


    driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
    neo4jSession = driver.session()
        
    cypher = "CALL apoc.periodic.iterate('MATCH (n) RETURN id(n) as id', 'MATCH (n) WHERE id(n) = id DETACH DELETE n', {batchSize:1000})"
    results = neo4jSession.run(cypher)  


    json_result = json.dumps({})
    return flask.Response(response=json_result, status=200, content_type='application/json')




    