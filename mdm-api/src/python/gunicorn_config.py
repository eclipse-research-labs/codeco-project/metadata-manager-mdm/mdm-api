"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

bind = '0.0.0.0:8090'
timeout = 3600

workers = 1
worker_class = 'gthread'
threads = 80
loglevel = 'debug'
