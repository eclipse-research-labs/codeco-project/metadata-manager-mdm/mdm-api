"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import json
import flask
import logging
from neo4j import GraphDatabase
import connexion

import config


def getEntityType():
    try:
        driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
        neo4jSession = driver.session()
        cypher = "MATCH (n) RETURN distinct labels(n) as LABELS"
        results = neo4jSession.run(cypher)
        print(results)
    except Exception as error:
        print(error)
        n4j = str(error)
    
    neo4jSession = driver.session()
    cypher = "MATCH (n) RETURN distinct labels(n) as LABELS"
    results = neo4jSession.run(cypher)
    entityType = []
    for x in results.data(): 
        typeInfo = {}
        typeInfo[x["LABELS"][0]] = []
        cypher = "MATCH (p:" + x["LABELS"][0] + ") WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS attr RETURN attr"
        resultsAttr = neo4jSession.run(cypher)
        for attr in resultsAttr:
            typeInfo[x["LABELS"][0]].append(attr["attr"])
        entityType.append(typeInfo)
        # cypher = "MATCH (p:process) WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN allfields"
    json_result = json.dumps(entityType)
    return flask.Response(response=json_result, status=200, content_type='application/json')


def getEntities(entitytype):
    driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
    neo4jSession = driver.session()
    cypher = "MATCH (n:" + entitytype + ") RETURN n"
    results = neo4jSession.run(cypher)
    entities = []
    for x in results.data():
        entities.append(x["n"])
    res={}
    res["limit"] = connexion.request.args.get("limit") # connexion.request.args.get("limit",10 )
    res["page"] = connexion.request.args.get("offset")
    res["max"] = connexion.request.args.get("offset")
    res["data"] = entities
    logging.info("--------")
    for key in connexion.request.args:
       logging.info(key) 
    logging.info("--------")   
    json_result = json.dumps({entitytype:entitytype})

    json_result = json.dumps(res)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getEntitiesAttributes(entitytype):
    driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
    neo4jSession = driver.session()
    cypher = "MATCH (p:" + entitytype + ") WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS attr RETURN attr"
    resultsAttr = neo4jSession.run(cypher)
    typeInfo = []
    for attr in resultsAttr:
        typeInfo.append(attr["attr"])
    # cypher = "MATCH (p:process) WITH DISTINCT keys(p) AS keys UNWIND keys AS keyslisting WITH DISTINCT keyslisting AS allfields RETURN allfields"
    json_result = json.dumps(typeInfo)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def graphpath(edf_id):
    # https://stackoverflow.com/questions/41646763/neo4j-how-to-find-distinct-one-hop-paths-based-on-the-weight-of-the-relationshi
    driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
    neo4jSession = driver.session()
    nodes = []
    relations = []

    cypher = "MATCH path=(n{edf_id:'" + edf_id +"'})-[r*.." + str(connexion.request.args.get("hops",1)) +  "]-(c) "
    cypher = cypher + "WITH relationships(path) AS rels "
    cypher = cypher + "UNWIND rels AS rel "
    cypher = cypher + "WITH DISTINCT rel "
    cypher = cypher + "RETURN rel, TYPE(rel) as TYPE, id(startNode(rel)) as start , id(endNode(rel)) as end"
    results = neo4jSession.run(cypher)
    logging.info(cypher)
    for x in results.data():
        relObj ={}
        relObj["LABEL"] = x["rel"][1]
        relObj["FROM"] = x["end"]
        relObj["TO"] = x["start"]
        relations.append(relObj)
        pass


    cypher = "MATCH path=(n{edf_id:'" + edf_id +"'})-[r*.." + str(connexion.request.args.get("hops",1)) +  "]-(c) "
    cypher = cypher + "WITH nodes(path) AS nodes "
    cypher = cypher + "UNWIND nodes AS node "
    cypher = cypher + "WITH DISTINCT node "
    cypher = cypher + "RETURN node,id(node) as id ,LABELS(node) as labels"
    results = neo4jSession.run(cypher)
    for x in results.data():
        nodeObj = x["node"]
        nodeObj["ID"] = x["id"]
        nodeObj["LABEL"] = x["labels"][0]
        nodes.append(nodeObj)
        pass

    data = {}
    data["nodes"] = nodes
    data["relations"] = relations
    json_result = json.dumps(data)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def cypher(body):
    driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
    neo4jSession = driver.session()

    results = neo4jSession.run(body.decode("utf-8"))
    data = results.data()
    json_result = json.dumps(data)
    return flask.Response(response=json_result, status=200, content_type='application/json')