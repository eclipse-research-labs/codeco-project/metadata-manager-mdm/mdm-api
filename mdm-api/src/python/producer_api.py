"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import json
import flask
import logging
import ssl

from kafka import KafkaProducer
from kafka.errors import KafkaError

import config



kafkaProducer = None
isKafkaConnected = False
#except Exception as e: print(e)

def kafkaConnect():
    global kafkaProducer
    sasl_mechanism = 'SCRAM-SHA-512'
    security_protocol = 'SASL_SSL'
    # Create a new context using system defaults, disable all but TLS1.2
    context = ssl.create_default_context()
    context.options &= ssl.OP_NO_TLSv1
    context.options &= ssl.OP_NO_TLSv1_1
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    try:
        kafkaProducer = KafkaProducer(
                 bootstrap_servers= config.KAFKA_BROKER,
                 sasl_plain_username = config.KAFKA_USER,
                 sasl_plain_password = config.KAFKA_PASSWORD,
                 ssl_context = context,
                 security_protocol = security_protocol,
                 sasl_mechanism = sasl_mechanism
                 )
        return True
    except Exception as e:
        logging.error(e)
    return False
    

    

def producerPost(body):
    config.mdm_producer_metric_summary.time()

    eventStructurErrorText = ""
   
    # check event
    eventTypeStructure = True
    if "connector_edf_id" not in body:
        eventTypeStructure = False
    if "connector_type" not in body:
        eventTypeStructure = False
    if "event_type" not in body:
        eventTypeStructure = False
    if "payload" not in body:
        eventTypeStructure = False

    # check payload
    payloadStructure = True
    logging.info("1")
    if eventTypeStructure:
        logging.info("2")
        if "json_schema_ref" not in body["payload"]:
            logging.info("3")
            payloadStructure = False
        if "edf_id" not in body["payload"]:
            payloadStructure = False
            if (("from_edf_id" in body["payload"]) and ("to_edf_id" in body["payload"])):
                payloadStructure = True
                eventType = "SystemRelationship"
                if "edf_id" in body["payload"]:
                    payloadStructure = False
        else:
            eventType = "SystemEntity"
            if (("from_edf_id" in body["payload"]) or ("to_edf_id" in body["payload"])):
                payloadStructure = False    
    else:
        payloadStructure = False
        logging.info("4")

    responce = {}
    responce["event"] = eventTypeStructure
    responce["payload"] = payloadStructure
    if (not eventTypeStructure) or (not payloadStructure):
        responce["error"] = "eventstructure wrong"
        status = 400    
    else:
        responce = {}
        status = 200
 
        kafkaKey={}
        # build Kafka Key
        if eventType  == "SystemEntity":
            kafkaKey["connector_edf_id"] = ["connector_edf_id"]
            kafkaKey["json_schema_ref"] = body["payload"]["json_schema_ref"]
            kafkaKey["edf_id"] = body["payload"]["edf_id"]
        if eventType == "SystemRelationship":
            kafkaKey["connector_edf_id"] = body["connector_edf_id"]
            kafkaKey["json_schema_ref"] = body["payload"]["json_schema_ref"]
            kafkaKey["from_edf_id"] = body["payload"]["from_edf_id"]
            kafkaKey["to_edf_id"] = body["payload"]["to_edf_id"]

        """
        sasl_mechanism = 'SCRAM-SHA-512'
        security_protocol = 'SASL_SSL'
        # Create a new context using system defaults, disable all but TLS1.2
        context = ssl.create_default_context()
        context.options &= ssl.OP_NO_TLSv1
        context.options &= ssl.OP_NO_TLSv1_1
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        kafkaProducer = KafkaProducer(
                     bootstrap_servers= config.KAFKA_BROKER,
                     sasl_plain_username = config.KAFKA_USER,
                     sasl_plain_password = config.KAFKA_PASSWORD,
                     ssl_context = context,
                     security_protocol = security_protocol,
                     sasl_mechanism = sasl_mechanism
                     )
        """

        kafkaProducer.send(config.KAFKA_TOPIC, key=bytes(json.dumps(kafkaKey), encoding="utf-8"), value=bytes(json.dumps(body), encoding="utf-8"))
        kafkaProducer.flush()


    config.mdm_producer_metric_count.labels(eventType=body["event_type"],
                                      entityType=eventType, 
                                      entity=body["payload"]["json_schema_ref"]).inc()

    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=status, content_type='application/json')

def producerGet():
    json_result = json.dumps({'rolecheck': '"pf_admins 2"'})
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getSchemaVersionByURI(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getSchemaGroups(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getGroup(groupid):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def createGroup(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def deleteGroup(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getSchemasByGroup(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def deleteSchemasByGroup(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getLatestSchema(groupid,schemaid):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def createSchema(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def deleteSchema(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getSchemaVersions(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def getSchemaVersion(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')

def deleteSchemaVersion(body):
    responce = {}
    json_result = json.dumps(responce)
    return flask.Response(response=json_result, status=200, content_type='application/json')




kafkaConnect()




