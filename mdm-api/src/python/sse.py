"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import flask
import connexion



# https://www.easydevguide.com/posts/flask_sse

characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'


# do not works
def getKafkaMessages(entities,relationships):
    return "aaa"


def sseKafka():
    return flask.Response(
        getKafkaMessages(connexion.request.args.getlist("entities"),connexion.request.args.getlist("relationships")),  # gen_date_time() is an Iterable
        mimetype='text/event-stream'  # mark as a stream response
    )