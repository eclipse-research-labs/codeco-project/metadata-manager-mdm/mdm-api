"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""

from connexion.exceptions import OAuthProblem
import requests
import jwt

import config
from cachetools import TTLCache

import kubernetes


cache = TTLCache(maxsize=config.TOKEN_CACHE_SIZE, ttl=config.TOKEN_CACHE_TTL)

def apikey_auth(token, required_scopes):
    global cache
    if config.OIDC_ENABLE:
        if str(token).startswith("Bearer "):
            if str(token) in cache:
                return {}
            else:
                jwtToken = jwt.decode(token[7:], options={"verify_signature": False})
                data = ("client_id=" + config.OIDC_CLIENT_ID +
                        "&client_secret=" + config.OIDC_CLIENT_SECRET +
                        "&token=" + token[7:])
                headers = {'Content-Type': 'application/x-www-form-urlencoded'}              
                r = requests.post(config.OIDC_INTROSPECT_URL, data=data, headers=headers, verify=False)
                if r.status_code > 200 :
                    raise OAuthProblem('Invalid token')
                if r.json()["active"] == True:
                    cache[str(token)] = True
                    return {}
                else:
                    raise OAuthProblem('Token not active')
            
    if config.K8S_AUTH_ENABLE:
        if str(token).startswith("SAToken "):
            if str(token) in cache:
                return {}
            else:
                if config.K8S_AUTH_CONTROLL_SECRET == "notSet":
                    kubernetes.config.load_incluster_config()
                    api_instance = kubernetes.client.AuthenticationV1Api()
                else: 
                    k8sConfiguration = kubernetes.client.Configuration()
                    k8sConfiguration.host = config.K8S_HOST_URL
                    k8sConfiguration.verify_ssl = False
                    k8sConfiguration.api_key = {"authorization": "Bearer " + config.K8S_AUTH_CONTROLL_SECRET}
                    k8sApiClient = kubernetes.client.ApiClient(k8sConfiguration)
                    api_instance = kubernetes.client.AuthenticationV1Api(k8sApiClient)

                token[9:]
                spec = {"token":token[9:]}
                body = kubernetes.client.V1TokenReview(spec=spec)
                ret = api_instance.create_token_review(body)
                objData = ret.to_dict()
                print(objData)
                if objData['status']['authenticated']:
                    if 'system:serviceaccount:' + config.NAMESPACE + ':api-client' in objData['status']['user']['username']:
                        cache[str(token)] = True
                        return {}

        raise OAuthProblem('Invalid token')
    return {}


# https://documenter.getpostman.com/view/16815994/UVJcmwiY   --> user info