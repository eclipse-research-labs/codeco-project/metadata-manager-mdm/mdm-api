"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import os
import logging

from prometheus_client import Summary, Counter, generate_latest

mdm_producer_metric_count =  Counter('mdm_events', 'mdm published events', ['eventType','entityType','entity'])
mdm_producer_metric_summary = Summary('mdm_events_request_time', 'mdm publishing request time')
mdm_producer_metric_summary.observe(1)




# kafka 
# kafka direct
KAFKA_BROKER = os.getenv('KAFKA_BROKER')
KAFKA_TOPIC = os.getenv('KAFKA_TOPIC')
KAFKA_USER = os.getenv('KAFKA_USER')
KAFKA_PASSWORD = os.getenv('KAFKA_PASSWORD',"notSet")
if KAFKA_PASSWORD == "notSet":
    with open('/opt/app-root/src/mdm-kafka-jaas/client-passwords') as f:
        KAFKA_PASSWORD = f.readline().split(",")[0]


NEO4J_BOLT = os.getenv('NEO4J_BOLT')
NEO4J_AUTH = os.getenv('NEO4J_AUTH',"notSet")
if NEO4J_AUTH == "notSet":
    with open('/opt/app-root/src/mdm-neo4j-auth/NEO4J_AUTH') as f:
        NEO4J_AUTH = f.readline()

NEO4J_USER = NEO4J_AUTH.split("/")[0]
NEO4J_PW = NEO4J_AUTH.split("/")[1]
# system
LOGGING_LEVEL = os.getenv('LOGGING_LEVEL','debug')
LOGGING_LEVEL = "info"

# OIDC
OIDC_CLIENT_ID = os.getenv('OIDC_CLIENT_ID')
OIDC_CLIENT_SECRET = os.getenv('OIDC_CLIENT_SECRET')
OIDC_INTROSPECT_URL = os.getenv('OIDC_INTROSPECT_URL')
OIDC_ENABLE = bool(os.getenv('OIDC_ENABLE',"true").lower() == "true")


# K8s SA token Protect
K8S_AUTH_ENABLE=bool(os.getenv('K8S_AUTH_ENABLE',"true").lower() == "true")
K8S_AUTH_CONTROLL_SECRET=(os.getenv('K8S_AUTH_CONTROLL_SECRET',"notSet"))
K8S_HOST_URL=(os.getenv('K8S_HOST_URL',"notSet"))

if os.path.exists('/var/run/secrets/kubernetes.io/serviceaccount/namespace'):
    with open('/var/run/secrets/kubernetes.io/serviceaccount/namespace') as f:
        NAMESPACE = f.readline().split(",")[0]
else:
    NAMESPACE="mdm"

TOKEN_CACHE_SIZE = int(os.getenv('TOKEN_CACHE_SIZE',"100"))
TOKEN_CACHE_TTL = int(os.getenv('TOKEN_CACHE_TTL',"300"))


# set logging for all python components
numeric_level = getattr(logging, LOGGING_LEVEL.upper(), 10)
logging.basicConfig(format='%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=numeric_level)
#
