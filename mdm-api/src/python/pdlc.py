"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""

import json
import flask
import logging
from neo4j import GraphDatabase
import connexion

import config
import yaml

import random


def get():

    data = []

    cluster = connexion.request.args.get("cluster")
    namespace = connexion.request.args.get("namespace")
    pod = connexion.request.args.get("pod")
    rnd = connexion.request.args.get("rnd")

    driver = GraphDatabase.driver(config.NEO4J_BOLT,
                                  auth=(config.NEO4J_USER, config.NEO4J_PW))
    neo4jSession = driver.session()

    # this is for CIPHER sample
    # this is to be implementing
    cypher = 'MATCH(p:pod{name:"' + pod + '",namespace:"' + namespace + '"})-[]->(w:workernode)<-[]-(k:kubernetes)-[r3:complies]->(c:compliance_state)'
    cypher += ' WHERE toupper(k.name)="' + str(cluster).upper() + '"'
    cypher += ' RETURN p,w,k,c'
    results = neo4jSession.run(cypher)

    #logging.info(results.data())
    for x in results.data():
        podVal = (x["p"])
        workerVal = (x["w"])
        kubernetesVal = (x["k"])
        complianceVal = (x["c"])

    portability = "n/a"
    try:
        podStatus = json.loads(
            str(podVal["status"]).replace("'", '"').replace(
                ": None", ': "None"').replace(": True", ': "True"').replace(
                    ": False",
                    ': "False"').replace("datetime.datetime(",
                                         '"datetime.datetime(').replace(
                                             "tzinfo=tzlocal())",
                                             'tzinfo=tzlocal())"'))
        logging.info(podStatus["container_statuses"][0]["restart_count"])
        portability = str(
            1 / (1 + podStatus["container_statuses"][0]["restart_count"]))
    except Exception as e:
        logging.error("portability exception :" + str(e))

    compliance = "n/a"
    try:
        compliance = str(float(complianceVal["status"]) / 100)
    except Exception as e:
        logging.error("compliance exception :" + str(e))

    freshness = "n/a"
    try:
        cypher = 'MATCH (c:kubernetes)-[r1:contains]->(w:workernode)<-[r2:runs]-(p:pod {name: "' + pod + '",namespace:"' + namespace + '"})-[h:has]->(f:freshness_state) '
        cypher += ' WHERE toupper(c.name)="' + str(cluster).upper() + '"'
        cypher += ' RETURN f'
        results = neo4jSession.run(cypher)
        for x in results.data():
            freshness = str((x["f"])["value"])
    except Exception as e:
        logging.error("freshness exception :" + str(e))

    returnObj = {}
    returnObj["spec"] = {}
    returnObj["spec"]["groups"] = "codeco.com"
    returnObj["spec"]["versions"] = []

    returnObj["spec"]["scope"] = "Namespaced"
    returnObj["spec"]["names"] = {}
    returnObj["spec"]["names"]["plural"] = "mdm-mons"
    returnObj["spec"]["names"]["singular"] = "mdm-mon"
    returnObj["spec"]["names"]["kind"] = "MDM"
    returnObj["spec"]["names"]["shortNames"] = []
    returnObj["spec"]["names"]["shortNames"].append("mdm-m")

    versionsObject = {}
    versionsObject["name"] = "v1"
    versionsObject["served"] = "true"
    versionsObject["storage"] = "true"
    versionsObject["schema"] = {}
    versionsObject["schema"]["openAPIV3Schema"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["type"] = "object"
    versionsObject["schema"]["openAPIV3Schema"]["properties"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "type"] = "object"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"] = {}

    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["pod_name"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["pod_name"]["description"] = "identifier of the node"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["pod_name"]["type"] = "string"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["pod_name"]["value"] = pod

    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["freshness"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["freshness"][
            "description"] = "healthiness of the node based on data freshness"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["freshness"]["type"] = "string"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["freshness"]["value"] = freshness

    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["compliance"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["compliance"][
            "description"] = "compliance to application requirements"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["compliance"]["type"] = "string"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["compliance"]["value"] = compliance

    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["portability"] = {}
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["portability"][
            "description"] = "portability level for a specific application"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["portability"]["type"] = "string"
    versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
        "properties"]["portability"]["value"] = portability
    if rnd == "true":
        versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
            "properties"]["freshness"]["value"] = str(random.uniform(0, 1))
        versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
            "properties"]["compliance"]["value"] = str(random.uniform(0, 1))
        versionsObject["schema"]["openAPIV3Schema"]["properties"]["spec"][
            "properties"]["portability"]["value"] = str(random.uniform(0, 1))

    returnObj["spec"]["versions"].append(versionsObject)

    return flask.Response(response=yaml.dump(returnObj,
                                             allow_unicode=True,
                                             sort_keys=False),
                          status=200,
                          content_type='text/plain')
