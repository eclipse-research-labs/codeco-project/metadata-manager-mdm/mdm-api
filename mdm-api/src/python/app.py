"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

from flask import render_template
from flask_cors import CORS

import connexion
from prometheus_client import Counter, generate_latest

app = connexion.App(__name__, specification_dir="./")

CORS(app.app)
app.add_api("templates/swagger.yaml")

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/swagger.yaml")
def swagger():
    return render_template("swagger.yaml")

@app.route('/metrics')
def metrics():
    return generate_latest()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8090, debug=True)

