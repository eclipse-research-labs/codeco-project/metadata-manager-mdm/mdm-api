<!--
  ~ SPDX-License-Identifier: Apache-2.0
  ~ Copyright IBM Corp 2023
-->

# MDM CODECO

## Table of Contents
- [Overview](#overview)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [API](#api)
- [License](#license)

## Overview

## Prerequisites

## Install
- Set the environment variables:
```shellscript
export MDM_NAMESPACE=mdm
export MDM_CONTEXT=docker-desktop

kubectl --context=$MDM_CONTEXT create namespace  $MDM_NAMESPACE  
```
- Add HELM repositories:
```shellscript
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add neo4j https://helm.neo4j.com/neo4j
```
- Install zookeeper, kafka and neo4j:
```shellscript
helm --kube-context=$MDM_CONTEXT install mdm-zookeeper -n $MDM_NAMESPACE  bitnami/zookeeper  -f ./deployment/zookeeper-helm.yaml
helm --kube-context=$MDM_CONTEXT install mdm-kafka -n $MDM_NAMESPACE bitnami/kafka --version 21.1.1 -f ./deployment/kafka-helm.yaml
helm --kube-context=$MDM_CONTEXT install mdm-neo4j -n $MDM_NAMESPACE neo4j/neo4j-standalone -f ./deployment/neo4j-helm.yaml  
```
- Create the Kafka topic:
```shellscript
kubectl --context=$MDM_CONTEXT -n $MDM_NAMESPACE exec -i mdm-kafka-0 -- /opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server mdm-kafka-0:9093 --create --topic json-events --config cleanup.policy=compact
```

- Install the MDM controller:
If you need to change the tag of the docker image, or any other [value](./controller/src/helm/values.yaml), provide it in your own YAML file.
```shellscript
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install mdm-controller ./controller/src/helm [-f my_values.yaml]
```

- Install the MDM API:
If you need to change the tag of the docker image, or any other [value](./controller/src/helm/values.yaml), provide it in your own YAML file.
```shellscript
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install mdm-api ./mdm-api/src/helm [-f my_values.yaml]
```

- Expose the API:
If you expose the API service or have enabled an ingress you will find the following endpoints:
   - `http://<exposed url>:8090`                  minimal UI
   - `http://<exposed url>:8090/mdm/api/v1`       API basic path
   - `http://<exposed url>:8090/mdm/api/v1/ui/#`  Swagger UI 


### Local development
- configure Python env
- export the env vars:
```shellscript
export NEO4J_BOLT=bolt://localhost:7687
export NEO4J_AUTH=neo4j/xxxxxxx     
export KAFKA_BROKER=localhost:9092
export KAFKA_TOPIC=json-events
export KAFKA_USER=connector
export KAFKA_PASSWORD=xxxxxxxxxxxxx

# disable API endpoint authentication for local run
export OIDC_ENABLE=False
export K8S_AUTH_ENABLE=False
```
- expose kafka endpoint `kubectl --context=$MDM_CONTEXT -n $MDM_NAMESPACE port-forward mdm-kafka-0 9092:9092 `
- add the kafka endpoint to the `/etc/hosts` file `127.0.0.1       mdm-kafka-0.mdm-kafka-headless.mdm.svc.cluster.local`

- run the API in a terminal
```shellscript
cd ./mdm-api/src/python 
gunicorn --reload -c gunicorn_config.py wsgi:app --log-level debug --access-logfile -
```
- open browser `http://localhost:8090`
- run the controller in a terminal
```shellscript
cd ./controller/src/python
python ctrl.py
```

## API

The api definition is as [swagger](mdm-api/src/python/templates/swagger.yaml)  file in the project avaiable. 

## Prometheus
Prometeus metrics are on the same port as the api on the 
`metrics_path: /metrics`

## License

This MDM-API is released under the Apache 2.0 license.
The license's full text can be found in [LICENSE](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/metadata-manager-mdm/mdm-api/-/blob/main/LICENSE?ref_type=heads).
