"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""

import logging
from neo4j import GraphDatabase

import config


class PrjojectionLoader:
    def __init__(self):
        self.driver = GraphDatabase.driver(config.NEO4J_BOLT, auth=(config.NEO4J_USER, config.NEO4J_PW))
        self.neo4jSession = self.driver.session()

    def publishRelationship(self, event):
        if event["event_type"] == "upsert":
            cypher = "MATCH (f) WHERE f.edf_id='" + event["payload"]["from_edf_id"] + "' RETURN COUNT(f)"
            result = self.neo4jSession.run(cypher)
            if result.data()[0]["COUNT(f)"] == 0:
                cypher = "CREATE (f{edf_id:'" + event["payload"]["from_edf_id"] + "'})"
                result = self.neo4jSession.run(cypher)
            cypher = "MATCH (t) WHERE t.edf_id='" + event["payload"]["to_edf_id"] + "' RETURN COUNT(t)"
            result = self.neo4jSession.run(cypher)
            if result.data()[0]["COUNT(t)"] == 0:
                cypher = "CREATE (t{edf_id:'" + event["payload"]["to_edf_id"] + "'})"
                result = self.neo4jSession.run(cypher)
            cypher = "MATCH (f),(t) WHERE f.edf_id='" + event["payload"]["from_edf_id"] + "' AND t.edf_id='" + event["payload"]["to_edf_id"] + "' "
            cypher = cypher + "MERGE (f)-[r:" + event["payload"]["json_schema_ref"].split(":")[2] + "]->(t) "
            cypher = cypher + "SET r.from_edf_id='" + event["payload"]["from_edf_id"] + "' "
            cypher = cypher + "SET r.to_edf_id='" + event["payload"]["to_edf_id"] + "' "
            logging.info(cypher)
            result = self.neo4jSession.run(cypher) 
            pass
        if event["event_type"] == "delete":
            cypher = "MATCH (n)-[r]-(a) WHERE r.from_edf_id='" + event["payload"]["from_edf_id"] + "' AND "
            cypher = cypher + "r.to_edf_id='" + event["payload"]["to_edf_id"] + "' "
            cypher = cypher + "CALL { WITH r DELETE r} "
            cypher = cypher + "CALL { WITH a MATCH (a)  WHERE not (a)--() DELETE a }"
            cypher = cypher + "CALL { WITH n MATCH (n)  WHERE not (n)--() DELETE n }"
            logging.info(cypher)
            result = self.neo4jSession.run(cypher)
            pass
        return 0

    def publishEvent(self, event):
        if event["event_type"] == "upsert":
            cypher = "MATCH (f) WHERE f.edf_id='" + event["payload"]["edf_id"] + "' RETURN COUNT(f)"
            result = self.neo4jSession.run(cypher)
            if result.data()[0]["COUNT(f)"] == 0:
                cypher = "MERGE (n:" + event["payload"]["json_schema_ref"].split(":")[2] + "{edf_id:'" + event["payload"]["edf_id"] + "'}) SET "
            else:
                cypher = "MATCH (n) WHERE n.edf_id='" + event["payload"]["edf_id"] + "' SET n:" + event["payload"]["json_schema_ref"].split(":")[2] + ", "
            
            # cypher = "MERGE (n:" + event["payload"]["json_schema_ref"].split(":")[2] + "{edf_id:'" + event["payload"]["edf_id"] + "'}) SET "
            firstAttr = True
            for attr in event["payload"]:
                if attr != "json_schema_ref":
                    if firstAttr == False:
                        cypher = cypher + ", "
                    cypher = cypher + "n." + attr + "='" + str(event["payload"][attr]).replace("'","\\'") + "'"
                    firstAttr = False
            logging.info(cypher)
            result = self.neo4jSession.run(cypher) 
            
            pass
        if event["event_type"] == "delete":
            cypher = "MATCH (n)-[r]-(a) WHERE n.edf_id='" + event["payload"]["edf_id"] + "' DETACH DELETE n "
            cypher = cypher + "WITH a MATCH (a) WHERE not (a)--() DELETE a"
            logging.info(cypher)
            result = self.neo4jSession.run(cypher) 
            pass
        return 0