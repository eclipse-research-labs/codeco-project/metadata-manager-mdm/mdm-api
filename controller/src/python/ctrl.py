"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""
import importlib
import json
import logging
from kafka import KafkaConsumer, TopicPartition
import ssl
import time

import config

pythonLoderModul = config.PF_CONNECTOR_CTRL_MODULE
projectionLoader = importlib.import_module(pythonLoderModul)
loader = projectionLoader.PrjojectionLoader()

sasl_mechanism = 'SCRAM-SHA-512'
security_protocol = 'SASL_SSL'
# Create a new context using system defaults, disable all but TLS1.2
context = ssl.create_default_context()
context.options &= ssl.OP_NO_TLSv1
context.options &= ssl.OP_NO_TLSv1_1
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE
logging.info("Kafka direct mode")
logging.info("Bootstrap_servers: " + config.KAFKA_BROKER)
logging.info("Kafka user: " + config.KAFKA_USER)
logging.info("Kafka topic " + config.KAFKA_TOPIC)
consumer = KafkaConsumer(group_id=config.KAFKA_GROUP_ID,
                         bootstrap_servers=config.KAFKA_BROKER,
                         auto_offset_reset="earliest",
                         sasl_plain_username=config.KAFKA_USER,
                         sasl_plain_password=config.KAFKA_PASSWORD,
                         client_id=config.KAFKA_GROUP_ID,
                         ssl_context=context,
                         security_protocol=security_protocol,
                         sasl_mechanism=sasl_mechanism,
                         enable_auto_commit=False,
                         consumer_timeout_ms=10000)

partitions = None
while partitions is None:
    partitions = consumer.partitions_for_topic(config.KAFKA_TOPIC)
    logging.warning(config.KAFKA_TOPIC + " topic does not exists yet")
    time.sleep(10)

partitionAssignments = []
partitionsOffset = []
print(partitions)
for p in partitions:
    partitionsOffset.append(0)
    partitionAssignments.append(
        TopicPartition(config.KAFKA_TOPIC, partitionsOffset[p]))
consumer.assign(partitionAssignments)

i = 0
max_records = 1000
start = None
#while i < 794:
#while i < 10:
while True:

    messages = consumer.poll(timeout_ms=1000, max_records=max_records)
    #if start == None:
    #    start = timeit.default_timer()
    for tp, records in messages.items():
        pollrecords = []
        for record in records:
            i = i + 1
            logging.info("-------------")
            logging.info(record.topic)
            logging.info(record.partition)
            logging.info(record.offset)
            logging.info(record.key)
            logging.info(record.value)
            value = json.loads(record.value)
            logging.info(value)
            if "from_edf_id" in value["payload"] and "to_edf_id" in value[
                    "payload"]:
                logging.info("relationship")
                loader.publishRelationship(value)
            if "edf_id" in value["payload"]:
                logging.info("entity")
                loader.publishEvent(value)
            #ToDO store offset / partion for restart
            logging.info(i)

#stop = timeit.default_timer()
#logging.info(stop - start)
