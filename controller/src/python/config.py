"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""

import os
import logging
# Set this var to the class PrjojectionLoader python file
PF_CONNECTOR_CTRL_MODULE = "neo4jloader"
# kafka direct
KAFKA_BROKER = os.getenv('KAFKA_BROKER')
KAFKA_GROUP_ID = os.getenv('KAFKA_GROUP_ID')
KAFKA_TOPIC = os.getenv('KAFKA_TOPIC')
KAFKA_USER = os.getenv('KAFKA_USER')
KAFKA_PASSWORD = os.getenv('KAFKA_PASSWORD',"notSet")
if KAFKA_PASSWORD == "notSet":
    with open('/opt/app-root/src/mdm-kafka-jaas/client-passwords') as f:
        KAFKA_PASSWORD = f.readline().split(",")[0]


NEO4J_BOLT = os.getenv('NEO4J_BOLT')
NEO4J_AUTH = os.getenv('NEO4J_AUTH',"notSet")
if NEO4J_AUTH == "notSet":
    with open('/opt/app-root/src/mdm-neo4j-auth/NEO4J_AUTH') as f:
        NEO4J_AUTH = f.readline()

NEO4J_USER = NEO4J_AUTH.split("/")[0]
NEO4J_PW = NEO4J_AUTH.split("/")[1]

# system
LOGGING_LEVEL = os.getenv('LOGGING_LEVEL','debug')
LOGGING_LEVEL = "info"

# set logging for all python components
numeric_level = getattr(logging, LOGGING_LEVEL.upper(), 10)
logging.basicConfig(format='%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=numeric_level)

